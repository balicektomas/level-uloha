/* ======================================================================================
GULPFILE.js
====================================================================================== */

/*
REQUIRE TASKS
--------------------------------------------------------------------------------------- */
var gulp 		   = require('gulp'),
	 sass 		   = require('gulp-sass'),
	 autoprefixer  = require('gulp-autoprefixer'),
	 minifyCss 	   = require('gulp-minify-css'),
	 plumber 	   = require('gulp-plumber'),
	 gutil 		   = require('gulp-util');

/*
onError
--------------------------------------------------------------------------------------- */
var onError = function(err) {
      gutil.log(gutil.colors.red(err));
};


/*
SASS KOMPILACE
--------------------------------------------------------------------------------------- */
gulp.task('sass', function() {
      gulp.src('./sass/**/*.scss')
      .pipe(plumber({
            errorHandler: onError
      }))
      .pipe(sass({
            includePaths : ['_/sass/']
      }))
      .pipe(autoprefixer({
            browsers: ['last 2 version']
      }))
      .pipe(minifyCss())
      .pipe(gulp.dest('./dist/'))   // sem se uklada vysledne css
});



/*
WATCH
--------------------------------------------------------------------------------------- */
gulp.task('watch', function() {
  	gulp.watch('./sass/**/*.scss', ['sass']);
});


/*
DEFAULT TASK
--------------------------------------------------------------------------------------- */
gulp.task('default', ['sass', 'watch']);