$(document).ready(function() {

    // min. vyska nadpisu
    var minHeight = 0;
    var final = 0;
    $('.about-us h3').each(function(){
		var actual = $(this).height();
		if(actual > minHeight) {
			final = actual;
		}
	});
    $('.about-us h3').css('min-height', final + 'px');

    // formular - nacteni
    var object = JSON.parse(localStorage.getItem('comments'));
    var commentOutput = '';
    var now = new Date().getTime();
    var count = 0;
    $.each(object, function (i) {
    	commentOutput += '<div class="comment">';
    	var timeDiff = new Date(now - JSON.stringify(object[i].timestamp));
    	commentOutput += '<p class="date">' + formatMs(timeDiff) + '</p>';
    	commentOutput += '<p class="text">' + JSON.stringify(object[i].text).replace(/\"/g, '') + '</p>';
	    commentOutput += '</div>';
	    count++;
	});
    $('.comments .count').text(count);
    $('.comments-wrapper').append(commentOutput);
});

// fixni hlavicka
$(window).scroll(function() {
    var scroll = $(window).scrollTop();

    if(scroll >= 2) {
        $('body').addClass('fixed-header');
    }
    else {
    	$('body').removeClass('fixed-header');
    }
});

// nacteni filmu
$.ajax({
	type: 'GET',
	url: 'https://swapi.dev/api/films/?format=json',
	dataType: 'json',
	success: function(data) {
		var output = '<table class="table table-sm table-films"><tbody>';
		var results = data.results;
		$.each(results, function(i, item) {
			output += '<tr>';
			var number = i + 1;
		    var title = item.title;
		    var director = item.director;
		    var release_date = item.release_date;
		    output += '<td>' + number + '.<td>' + title + '</td><td>' + director + '</td><td>' + formatDate(release_date) + '</td>';
		    output += '</tr>';
		});
		output += '</tbody></table>';

		$('.films .loading').hide();
		$('.films .table-responsive').append(output);
    }, 
    error: function(jqXHR, textStatus, errorThrown) {
        console.log('Films loading problem (errorThrown): ' + jqXHR.status);
    }
});

// formatovani datumu vydani filmu
function formatDate(date) {
	var d = new Date(date),
	month = '' + (d.getMonth() + 1),
	day = '' + d.getDate(),
	year = d.getFullYear();
	if (month.length < 2) month = '0' + month;
	if (day.length < 2) day = '0' + day;
	return [day, month, year].join('.');
}

// formular - ulozeni noveho komentare
//window.localStorage.clear(); // DEL
function saveComment() {
	var timeNow = new Date().getTime();
	var inputComment = $('#comment').val();
	var newComment = {
		timestamp: timeNow,
		text: inputComment
	};
	var allComments = [];
	allComments.push(newComment);
	var comments = JSON.parse(localStorage.getItem('comments'));
	$.each(comments, function (i) {
	    allComments.push(comments[i]);
	});
	localStorage.setItem('comments', JSON.stringify(allComments));
}

// format rozdilu casu u komentare
function formatMs(ms) {
	var seconds = (ms / 1000).toFixed(0);
	var minutes = (ms / (1000 * 60)).toFixed(0);
	var hours = (ms / (1000 * 60 * 60)).toFixed(0);
	var days = (ms / (1000 * 60 * 60 * 24)).toFixed(0);
	if(seconds < 60) {
		return seconds + ' seconds ago';
	}
	else if(minutes < 60) {
		return minutes + ' minutes ago';
	}
	else if(hours < 24) {
		return hours + ' hours ago';
	}
	else {
		return days + ' days ago';
	}
}

// mapa
var mymap = L.map('map', {
    center: [50.470877, 16.0573705],
    zoom: 12,
    scrollWheelZoom: false,
    dragging: false,
    tap: false
});
L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw', {
	maxZoom: 18,
	attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors, ' + 'Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
	id: 'mapbox/streets-v11',
	tileSize: 512,
	zoomOffset: -1
}).addTo(mymap);
var LeafIcon = L.Icon.extend({
	options: {
		shadowUrl: 'img/marker-shadow.png',
		iconSize:     [38, 95],
		shadowSize:   [50, 64],
		iconAnchor:   [22, 94],
		shadowAnchor: [4, 62],
		popupAnchor:  [-3, -76]
	}
});
var markerIcon = new LeafIcon({iconUrl: 'img/marker.png'});
L.marker([50.470877, 16.0573705], {icon: markerIcon}).addTo(mymap).bindPopup('<b>Hello world!</b><br />I am a popup.').openPopup();
L.circle([50.475, 16.09], {
	color: 'red',
	fillColor: '#f03',
	fillOpacity: 0.5,
	radius: 400
}).addTo(mymap).bindPopup('I am a circle.');