# README #

Dobrý den,

tady je k dispozici testovací úloha. Její live demo běží zde - http://tomas-balicek.cz/test/level-uloha/

Na rovinu říkám, že jsem nesplnil zadání na 100%. Nepoužil jsem bundler Webpack. 

Ale to již uvidíte sami při zkoumání (jak je možné, že to vůbec funguje) a vyhodnocení. :-)

Přeji hezký den a budu se těšít na odpověď, ať už bude jakákoli.

Děkuji

S pozdravem
Tomáš Balíček
+420 731 526 934
www.tomas-balicek.cz